<?php

function remove_posts_menu() {
    remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
    //remove_menu_page('tools.php');
}
add_action('admin_menu', 'remove_posts_menu');

function themeslug_enqueue_style() {
	wp_enqueue_style( 'style', get_bloginfo('template_url').'/style.min.css', false );
	wp_enqueue_style( 'almoni', get_bloginfo('template_url').'/assets/fonts/almoni/almoni.css', false );
  wp_enqueue_script( 'landjs', get_bloginfo('template_url'). '/assets/js/land.js', array ( 'jquery' ), 1.0, true);
	//wp_enqueue_style( 'bootstrap-rtl', get_bloginfo('template_url').'/assets/css/bootstrap-rtl.min.css', false );
}

add_action( 'wp_enqueue_scripts', 'themeslug_enqueue_style' );

function edit_admin_menus() {
    global $menu;
    global $submenu;

    $menu[20][0] = 'דפי נחיתה';
    $submenu['edit.php?post_type=page'][5][0] = 'כל דפי הנחיתה';
    $submenu['edit.php?post_type=page'][10][0] = 'דף נחיתה חדש';
}
add_action( 'admin_menu', 'edit_admin_menus' );

remove_theme_support('post-formats');
remove_theme_support('post-thumbnails');
remove_theme_support('custom-background');
remove_theme_support('custom-header');
remove_theme_support('automatic-feed-links');

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

function gzip_enable( $rules )
{
$my_content = <<<EOD
<IfModule mod_expires.c>
ExpiresActive On
ExpiresByType text/css "access 1 month"
ExpiresByType text/html "access 1 month"
ExpiresByType application/pdf "access 1 month"
ExpiresByType text/x-javascript "access 1 month"
ExpiresByType application/x-shockwave-flash "access 1 month"
ExpiresByType image/x-icon "access 1 year"
ExpiresDefault "access 1 month"
</IfModule>
## EXPIRES CACHING ##

<IfModule mod_deflate.c>
  # Compress HTML, CSS, JavaScript, Text, XML and fonts
  AddOutputFilterByType DEFLATE application/javascript
  AddOutputFilterByType DEFLATE application/rss+xml
  AddOutputFilterByType DEFLATE application/vnd.ms-fontobject
  AddOutputFilterByType DEFLATE application/x-font
  AddOutputFilterByType DEFLATE application/x-font-opentype
  AddOutputFilterByType DEFLATE application/x-font-otf
  AddOutputFilterByType DEFLATE application/x-font-truetype
  AddOutputFilterByType DEFLATE application/x-font-ttf
  AddOutputFilterByType DEFLATE application/x-javascript
  AddOutputFilterByType DEFLATE application/xhtml+xml
  AddOutputFilterByType DEFLATE application/xml
  AddOutputFilterByType DEFLATE font/opentype
  AddOutputFilterByType DEFLATE font/otf
  AddOutputFilterByType DEFLATE font/ttf
  AddOutputFilterByType DEFLATE image/svg+xml
  AddOutputFilterByType DEFLATE image/x-icon
  AddOutputFilterByType DEFLATE text/css
  AddOutputFilterByType DEFLATE text/html
  AddOutputFilterByType DEFLATE text/javascript
  AddOutputFilterByType DEFLATE text/plain
  AddOutputFilterByType DEFLATE text/xml

  # Remove browser bugs (only needed for really old browsers)
  BrowserMatch ^Mozilla/4 gzip-only-text/html
  BrowserMatch ^Mozilla/4\.0[678] no-gzip
  BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
  Header append Vary User-Agent
</IfModule>
EOD;
    return $rules . "\n" . $my_content;
}
add_filter('mod_rewrite_rules', 'gzip_enable');


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'הגדרות דף הנחיתה',
		'menu_title'	=> 'הגדרות דף הנחיתה',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'מעקב אנליטי והמרות',
		'menu_title'	=> 'מעקב אנליטי והמרות',
    'menu_slug' 	=> 'theme-analytics',
		'parent_slug'	=> 'theme-general-settings',
	));

}


$landingoptions = get_fields('options');

if ($landingoptions['gtm']){
  function gtm_head() {
    global $landingoptions;?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','<?php echo $landingoptions['gtm_id'];?>');</script>
    <!-- End Google Tag Manager -->
    <?php
  }
  add_action('wp_head', 'gtm_head');
  function gtm_foot() {
    global $landingoptions;?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $landingoptions['gtm_id'];?>"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php
  }
  add_action('wp_footer', 'gtm_foot');
}

if ($landingoptions['floating_call'] || $landingoptions['floating_whatsapp']){

  function floating_call() {
    global $landingoptions;
    ?>
    <div id="MobBtns">
      <style>
      #MobBtns{display:none;position:fixed;bottom:3px;left:2.5px;z-index:99;flex-direction:column-reverse}#MobBtns a{width:40px;height:40px;border-radius:50%;margin:2.5px 0;position:relative;display:flex;align-items:center;justify-content:center}#MobBtns a svg{fill:#fff;width:60%;height:60%}#MobBtns a.float_call{background:<?php echo $landingoptions['floating_call_color'];?>}#MobBtns a.float_whatsapp{background:<?php echo $landingoptions['whatsalp_color'];?>}@media (max-width:749px),(max-device-width:749px){#MobBtns{display:flex}}
      </style>
      <?php if ($landingoptions['floating_call']) {?>
        <a class="float_call" href="tel:<?php echo $landingoptions['click_phone'];?>">
          <img class="svg" src="<?php bloginfo('template_url');?>/assets/image/Mphone.svg" alt="">
        </a>
      <?php } if ($landingoptions['floating_whatsapp']) { ?>
        <a class="float_whatsapp" href="https://api.whatsapp.com/send?phone=<?php echo $landingoptions['whatsalp_phone'];?>&text=<?php echo urlencode($landingoptions['whatsalp_msg']);?>">
          <img class="svg" src="<?php bloginfo('template_url');?>/assets/image/whatsapp.svg" alt="">
        </a>
      <?php } ?>
    </div>
    <?php
  }
  add_action('wp_footer', 'floating_call');
}

if ($landingoptions['analytics_tracking']){

  function analytics() {
    global $landingoptions;
    ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $landingoptions['analytics_ua'];?>"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments)};
      gtag('js', new Date());
      <?php if ($landingoptions['optimize']) {?>
        gtag('config', '<?php echo $landingoptions['analytics_ua'];?>', { 'optimize_id': '<?php echo $landingoptions['optimize_id'];?>'});
      <?php } else {?>
        gtag('config', '<?php echo $landingoptions['analytics_ua'];?>');
      <?php }?>
    </script>
    <?php
  }

  add_action('wp_head', 'analytics');

  if ($landingoptions['analytics_cf7']){
    function analytics_cf7(){
      ?>
      <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            if(typeof gtag=="function") gtag('event', 'generate_lead', {
              'event_category': 'השארת פרטים',
              'event_action': 'שליחה'
            });
        }, false );
      </script>
      <?php
    }
    add_action('wp_footer', 'analytics_cf7');
  }
}

if ($landingoptions['facebook_tracking']){

  function pixel() {
    global $landingoptions;
    ?>
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '<?php echo $landingoptions['facebook_pixel'];?>');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=<?php echo $landingoptions['facebook_pixel'];?>&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    <?php
  }
  add_action('wp_head', 'pixel');

  if ($landingoptions['facebook_cf7']){
    function facebook_cf7(){
      ?>
      <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            if(typeof fbq=="function") fbq('track', 'Lead');
        }, false );
      </script>
      <?php
    }
    add_action('wp_footer', 'facebook_cf7');
  }
}

if ($landingoptions['adwords_tracking']){

  function adwords_call_conv() {
    global $landingoptions;
    echo $landingoptions['adwords_call'];
  }
  add_action('wp_head', 'adwords_call_conv');

  function adwords_call_num_conv() {
    global $landingoptions;
    ?>
    <script type="text/javascript">
    var initTels=function(e){var a=document.querySelectorAll('a[href^="tel:"');if(!a){return false}for(var c=0;c<a.length;c++){if(a[c].className==""){a[c].className+="tel-link"}else{a[c].className+=" tel-link"}var d=a[c].children;if(d.length>0){for(var b=0;b<d.length;b++){if(a[c].children[b].tagName=="SPAN"||a[c].children[b].tagName=="P"){if(a[c].children[b].innerText!=""&&a[c].children[b].children.length=="0"){if(/\d+-\d+/.test(a[c].children[b].innerText)){if(a[c].children[b].className==""){a[c].children[b].className+="tel-phone"}else{a[c].children[b].className+=" tel-phone"}}}}}}else{if(a[c].innerText!=""){if(/\d+-\d+/.test(a[c].innerText)){if(a[c].className==""){a[c].className+="tel-phone"}else{a[c].className+=" tel-phone"}}}}}changeNum(e)};var Adcallback=function(b,e){var a=document.getElementsByClassName("tel-link");var c=document.getElementsByClassName("tel-phone");for(var d=0;d<a.length;d++){a[d].href="tel:"+e}for(var d=0;d<c.length;d++){c[d].innerText=b}};var changeNum=function(a){if(typeof _googWcmGet==="function"){_googWcmGet(Adcallback,a)}};
    initTels('<?php echo $landingoptions['adwords_call_number'];?>');
    </script>
    <?php
  }
  add_action('wp_footer', 'adwords_call_num_conv');
}

if ($landingoptions['click_fraud']){

  function easyleads_click_fraud() {
    global $landingoptions;
    ?>
    <script>
    /*<![CDATA[ */
    var search_params = window.location.search;
    var customer_id = '<?php echo $landingoptions['click_fraud_id'];?>';
    /* ]]> */
    </script>
    <script type="text/javascript" async src="//app.adscale.com/click.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="display:none;border-style:none;" alt="" src="//clk.easyleads.com/Click?customer_id=bXxnaQ==" />
    </div>
    </noscript>
    <?php
  }
  add_action('wp_footer', 'easyleads_click_fraud');
}

if ($landingoptions['sms_activate']){
  function sms_cf7(){
    ?>
    <script type="text/javascript">
      document.addEventListener('wpcf7mailsent', function( event ) {
        jQuery.ajax({
            url : "<?php echo admin_url('admin-ajax.php'); ?>",
            type : "post",
            data : {
                "action": "sendleosms"
            }
          });
      }, false );
    </script>
    <?php
  }
  add_action('wp_footer', 'sms_cf7');
}

add_action( 'wp_ajax_sendleosms', 'sendleosms' );
add_action( 'wp_ajax_nopriv_sendleosms', 'sendleosms' );

function sendleosms(){
  global $landingoptions;
  $ch = curl_init();
  $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
  curl_setopt($ch, CURLOPT_URL,"https://ppc.leos.co.il/smser/leosms.php");
  curl_setopt($ch, CURLOPT_USERAGENT, $agent);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
    'key'=>'',
    'origin'=>get_bloginfo('url'),
    'rec'=>$landingoptions['sms_phone'],
    'msg'=>'התקבל ליד חדש מדף הנחיתה, נא לבדוק קבלתו וטיפולו.'
  ]));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $server_output = curl_exec ($ch);
  curl_close ($ch);
  wp_die();
}

?>
